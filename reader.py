from pprint import pprint
import sys
import csv
import pandas as pd

class Data_Reading_And_Saving:
    def __init__(self, src, dst, column, value_X, text_to_change):

        self.src = src
        self.dst = dst
        self.column = int(column)
        self.value_X = int(value_X)
        self.text_to_change = text_to_change

        self.input_data = []
        self.output_data = []
  
    def Read_and_Edit_file(self):
        file = pd.read_csv(self.src)
        pprint(file)

        df =file.iloc[self.value_X, self.column]
        print(df)
        
        df_change = file.replace([df], self.text_to_change)
        print(df_change)
        self.output_data.append(df_change)
        print(self.output_data)
        
    def Save_to_file(self):
        with open(self.dst, "w") as file_to_save:
            writer = csv.writer(file_to_save)        
            for row in self.output_data:
                writer.writerow(row)

    def Process(self):
        self.Read_and_Edit_file()
        self.Save_to_file()

def Validate(argv):
    if len(argv) < 5:
        raise Exception("Podano za mało parametrów.")
    elif len(argv) > 7:
        raise Exception("Podano za dużo parametrów.")
     
    try:
        int(argv[3])
    except ValueError:
        raise ValueError("Wartość nie jest liczbą.")
    
    if argv[1].split(".")[1] != "csv" and argv[2].split(".")[1] != "csv":
        raise Exception("Plik ma złe rozszerzenie.")

def Parser(argv):
    Validate(argv)

    if len(argv) != 6:
        return (argv[1], argv[2], argv[3], argv[4], None) 
    return (argv[1], argv[2], argv[3], argv[4], argv[5])
  
src, dst, column, value_X, text_to_change = Parser(sys.argv)

filter = Data_Reading_And_Saving(src, dst, column, value_X, text_to_change)

filter.Process()